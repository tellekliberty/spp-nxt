import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './app/core/header/header.component';
import { SidenavComponent } from './app/core/sidenav/sidenav.component';
import { MultiTrayComponent } from './app/core/multi-tray/multi-tray.component';
import { MenuDirectiveComponent } from './app/shared/menu-directive/menu-directive.component';
import { MultiTrayItemDirectiveComponent } from './app/shared/multi-tray-item-directive/multi-tray-item-directive.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SidenavComponent,
    MultiTrayComponent,
    MenuDirectiveComponent,
    MultiTrayItemDirectiveComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
