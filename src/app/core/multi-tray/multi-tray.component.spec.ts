import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MultiTrayComponent } from './multi-tray.component';

describe('MultiTrayComponent', () => {
  let component: MultiTrayComponent;
  let fixture: ComponentFixture<MultiTrayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultiTrayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultiTrayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
