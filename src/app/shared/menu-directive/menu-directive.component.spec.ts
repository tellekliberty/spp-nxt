import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuDirectiveComponent } from './menu-directive.component';

describe('MenuDirectiveComponent', () => {
  let component: MenuDirectiveComponent;
  let fixture: ComponentFixture<MenuDirectiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuDirectiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuDirectiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
