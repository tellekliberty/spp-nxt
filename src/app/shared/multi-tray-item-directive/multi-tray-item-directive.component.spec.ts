import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MultiTrayItemDirectiveComponent } from './multi-tray-item-directive.component';

describe('MultiTrayItemDirectiveComponent', () => {
  let component: MultiTrayItemDirectiveComponent;
  let fixture: ComponentFixture<MultiTrayItemDirectiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultiTrayItemDirectiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultiTrayItemDirectiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
